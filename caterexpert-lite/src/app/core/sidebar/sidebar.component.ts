import { Component, OnInit ,HostListener} from '@angular/core';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { BatchprintComponent } from 'src/app/events/dialog/batchprint/batchprint.component';
import { ApiService } from 'src/app/service/api.service';
import { FinalizeMenuEditComponent } from '../finalize-menu-edit/finalize-menu-edit.component';
import { ProposalImagesComponent } from '../proposal-images/proposal-images.component';
import { SendMailComponent } from '../send-mail/send-mail.component';
import { ServiceInfoComponent } from 'src/app/events/dialog/service-info/service-info.component';
// declare var $: any;
import $ from 'jquery'
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
 public  color='';
  public visibleSidebar2;
  constructor(public apiservice:ApiService,public dialogService: DialogService) { }
 public sideBarMenu=[
  // {id:18,tooltip:'Services Info',icon:'',click:this.serviceinfo()},
  // {id:18,tooltip:'Batch Print',icon:'',click:this.batchprint()},
  //  {id:17,tooltip:'Send Mail',icon:'',click:this.sendmail()},
  //  {id:16,tooltip:'Proposal Images',icon:'',click : this.proposalImages()},
  //  {id:15,tooltip:'Finalize Menu Edit',icon:'',click : this.show()},
  //  {id:14,tooltip:'Inquiry',icon:'local_phone',route:'/events/inquiry'},
   {id:13,tooltip:'Bill Service',icon:'attach_money',route:'/bill-service'},

  //  {id:12,tooltip:'eCaterXpert Orders',icon:'rice_bowl',route:''},
   {id:11,tooltip:'Approvals',icon:'done_all',route:'/approvals'},
   {id:10,tooltip:'Closed Days',icon:'event_busy',route:'/closed-days'},
   {id:9,tooltip:"View Call Log",icon:'description',route:'/view-call-log'},
   {id:8,tooltip:'Log Call',icon:'call',route:'/call-log'},
   {id:7,tooltip:'Create Inquiry',icon:'flag',route:'/events/inquiry'},
   {id:6,tooltip:'Create Event',icon:'event',route:'/events/create-event'},
   {id:5,tooltip:'Edit Customer',icon:'person_add',route:'/customer/customertabs'},
   {id:5,tooltip:'Create Contact',icon:'person_add',route:'/customer/createcontact'},
   {id:5,tooltip:'Create Customer',icon:'person_add',route:'/c-customer'},
   { tooltip:'Full order view',route:'/events/fullorder-view',icon:'flag'},


   {id:4,tooltip:'Customer Search',icon:'search',route:'/customer'},
   {id:3,tooltip:'Event orders',icon:'event_note',route:'/events/events-order'},
  //  {id:2,tooltip:'Summary Calendar View',icon:'calendar_view_month'},
   {id:1,tooltip:'Event List',icon:'format_list_bulleted',route:'/events/event-list'},
  ]
  screenHeight
  ngOnInit(): void {
    this.screenHeight = window.innerHeight;
    var height = this.screenHeight - 74;
    $('.layout-sidebar').css('height', height + 'px');
  }


	@HostListener('window:resize', ['$event'])
	onResize(event) {
    this.screenHeight = window.innerHeight;
    var height = this.screenHeight - 74;
    $('.layout-sidebar').css('height', height + 'px');
	}
  ref: DynamicDialogRef;

  show() {
      this.ref = this.dialogService.open(FinalizeMenuEditComponent, {
          header: 'Finalize Menu Edit',
          width: '90%',
          contentStyle: {"overflow": "auto"},
          baseZIndex: 10000,
          styleClass:'common-dialog-height'
      });
  }
  proposalImages(){
    this.ref = this.dialogService.open(ProposalImagesComponent, {
      header: 'Proposal Images For Event # 2229',
      width: '60vw',
      contentStyle: {"overflow": "auto"},
      baseZIndex: 10000,
      styleClass:'common-dialog-height'
  });
  }
  sendmail(){
    this.ref = this.dialogService.open(SendMailComponent, {
      header: 'Send Mail',
      width: '50vw',
      contentStyle: {"overflow": "auto"},
      baseZIndex: 10000,
      styleClass:'common-dialog-height'
  });
  }
  batchprint(){
    this.ref = this.dialogService.open(BatchprintComponent, {
      header: 'Batch Print',
      width: '30vw',
      contentStyle: {"overflow": "auto"},
      baseZIndex: 1000,
      styleClass:'common-dialog-height'
  });
  }
  serviceinfo(){
    this.ref = this.dialogService.open(ServiceInfoComponent, {
      header: 'Service Info',
      width: '80vw',
      contentStyle: {"overflow": "auto"},
      baseZIndex: 1000,
      styleClass:'common-dialog-height'
  });
  }
  ngOnDestroy() {
      if (this.ref) {
          this.ref.close();
      }
  }
}
