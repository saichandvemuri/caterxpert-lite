import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-e-caterxpert-orders',
  templateUrl: './e-caterxpert-orders.component.html',
  styleUrls: ['./e-caterxpert-orders.component.scss']
})
export class ECaterxpertOrdersComponent implements OnInit {

  customer
  constructor(private _service: DataService, private apiservice: ApiService) {
    this.data();
    this.apiservice.SetheaderName('eCaterXpert Orders')
  }

  ngOnInit(): void {
  }
  data() {
    this._service.getdata().subscribe((res) => {
      console.log(res)
      this.customer = res;
    })
  }

}
