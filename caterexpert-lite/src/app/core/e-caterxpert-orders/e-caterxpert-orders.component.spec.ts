import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ECaterxpertOrdersComponent } from './e-caterxpert-orders.component';

describe('ECaterxpertOrdersComponent', () => {
  let component: ECaterxpertOrdersComponent;
  let fixture: ComponentFixture<ECaterxpertOrdersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ECaterxpertOrdersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ECaterxpertOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
