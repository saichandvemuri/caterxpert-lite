import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-closed-days',
  templateUrl: './closed-days.component.html',
  styleUrls: ['./closed-days.component.scss']
})
export class ClosedDaysComponent implements OnInit {

  public currentindex:number=0;
  constructor(private apiservice:ApiService) {
    this.apiservice.SetheaderName('Closed Days')
   }
  ngOnInit(): void {
  }

   /**
   * onTabChange Method is used to activate corrosponding component
   */
  public onTabChange(event) {
    this.currentindex=event.index;

  }

}
