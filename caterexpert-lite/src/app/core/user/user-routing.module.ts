import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { SwitchUserComponent } from './switch-user/switch-user.component';
import { SystemPrefernceComponent } from './system-prefernce/system-prefernce.component';
import { UserPrefernceComponent } from './user-prefernce/user-prefernce.component';

const routes: Routes = [
  {path:'userprefernce',component:UserPrefernceComponent},
  {path:'systemprefernce',component:SystemPrefernceComponent},
  {path:'switchuser',component:SwitchUserComponent},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
