import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-user-prefernce',
  templateUrl: './user-prefernce.component.html',
  styleUrls: ['./user-prefernce.component.scss']
})
export class UserPrefernceComponent implements OnInit {

  public userPrefernceResponse: any;
  public filterPrefernceResponse: any;
  constructor(private apiservice: ApiService) {
    this.apiservice.SetheaderName('User Preferences')
  }

  public accountExecutives = [];
  public businessunits = [];
  public customerName = '';
  public eventCode = null;
  public eventType = null;
  public nextVal = 0;
  public pastVal = 0;
  public partyStatus = [];


  public userEventStatusId=null;
  public userEventTypeId=null;
  public userCountyId=null;
  public languageId=null;
  ngOnInit(): void {
    this.getUserPreferences();
    this.getFilterPreference();
  }



  public getUserPreferences() {
    try {
      this.apiservice.getUserPreferences().subscribe(res => {
        console.log(res)
        this.userPrefernceResponse = res.userPreferences;
        this.userPrefernceResponse.languagesList.map(x=>{
          if(x.culture==this.userPrefernceResponse.launageType){
            this.languageId=x.id;
          }
        })
        this.userEventStatusId=this.userPrefernceResponse.optStatusValue==0?null:this.userPrefernceResponse.optStatusValue;
        this.userCountyId=this.userPrefernceResponse.cmbCountyId==0?null:this.userPrefernceResponse.cmbCountyId;
        this.userEventTypeId=this.userPrefernceResponse.eventType==0?null:this.userPrefernceResponse.eventType;


      })
    } catch (error) {

    }
  }
  public getFilterPreference() {
    try {
      this.apiservice.getFilterPreference().subscribe(res => {
        console.log(res);
        this.filterPrefernceResponse = res.filterPreference;
        this.pastVal = res.filterPreference.pastVal;
        this.nextVal = res.filterPreference.nextVal;
        this.eventCode=this.filterPrefernceResponse.eventCode==0?null:this.filterPrefernceResponse.eventCode;
        this.partyStatus = res.filterPreference.partyStatus.length>0?res.filterPreference.partyStatus.split(','):[]
        this.businessunits = res.filterPreference.businessunits.length>0?res.filterPreference.businessunits.split(','):[];
        this.accountExecutives = res.filterPreference.accountExec.length>0?res.filterPreference.accountExec.split(','):[]
        this.accountExecutives = this.accountExecutives.map(x => {
          return +x
        })
        this.businessunits = this.businessunits.map(x => {
          return +x
        })
        this.partyStatus = this.partyStatus.map(x => {
          return +x
        })

        console.log(this.partyStatus, this.businessunits, this.accountExecutives)
      })
    } catch (error) {

    }
  }
  public saveFilterPreference() {
    try {

      let obj = {
        "pastVal": +this.pastVal,
        "nextVal": +this.nextVal,
        "eventCode": this.eventCode==null?0:this.eventCode,
        "customerName": this.customerName,
        "userId": this.apiservice.loginResponse.loggedInUser,
        "eventTypeId": this.eventType==null?0:this.eventType,
        "partyStatus": this.partyStatus.toString(),
        "accountExecutives": this.accountExecutives.toString(),
        "businessUnits": this.businessunits.toString()

      }
      this.apiservice.saveFilterPreference(obj).subscribe(res => {
        console.log(res)
        Swal.fire('','Updated Successfully','success');
      })
    } catch (error) {
    }
  }
  public saveUserPreferences() {
    try {
      let obj =
      {
        "eventStatusId": this.userEventStatusId==null?0:this.userEventStatusId,
        "eventTypeId": this.userEventTypeId==null?0:this.userEventTypeId,
        "cmbCountyId": this.userCountyId==null?0:this.userCountyId,
        "languageId": this.languageId==null?0:this.languageId,
        "userId": this.apiservice.loginResponse.loggedInUser,
      }
      this.apiservice.saveUserPreferences(obj).subscribe(res => {
        console.log(res)
        Swal.fire('','Updated Successfully','success');

      })
    } catch (error) {

    }
  }
}
