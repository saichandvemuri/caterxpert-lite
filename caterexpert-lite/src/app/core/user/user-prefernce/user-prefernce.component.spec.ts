import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPrefernceComponent } from './user-prefernce.component';

describe('UserPrefernceComponent', () => {
  let component: UserPrefernceComponent;
  let fixture: ComponentFixture<UserPrefernceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserPrefernceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPrefernceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
