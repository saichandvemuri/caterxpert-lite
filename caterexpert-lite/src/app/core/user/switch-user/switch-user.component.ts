import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-switch-user',
  templateUrl: './switch-user.component.html',
  styleUrls: ['./switch-user.component.scss']
})
export class SwitchUserComponent implements OnInit {

  constructor(private apiservice: ApiService) {
    this.apiservice.SetheaderName('Superadmin Login')
  }

  public sidebar = []
  ngOnInit(): void {
    this.getAfterLoginValues();
  }
  public getAfterLoginValues() {
    try {
      this.apiservice.getAfterLoginValues().subscribe(res=>{
        console.log(res);
      })
    } catch (error) {

    }
  }
  public saveSuperAdminLoginAction(){
    try {
      let obj={}
      this.apiservice.saveSuperAdminLoginAction(obj).subscribe(res=>{
        console.log(res);
      })
    } catch (error) {

    }
  }
}
