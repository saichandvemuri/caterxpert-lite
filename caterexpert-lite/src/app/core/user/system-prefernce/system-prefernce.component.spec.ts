import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SystemPrefernceComponent } from './system-prefernce.component';

describe('SystemPrefernceComponent', () => {
  let component: SystemPrefernceComponent;
  let fixture: ComponentFixture<SystemPrefernceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SystemPrefernceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemPrefernceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
