import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-system-prefernce',
  templateUrl: './system-prefernce.component.html',
  styleUrls: ['./system-prefernce.component.scss']
})
export class SystemPrefernceComponent implements OnInit {
  public dateFormat=null;
  public catererName='';
  public systemprefernceResponse:any;
  constructor(private apiservice:ApiService) {
    this.apiservice.SetheaderName('System Preferences')
   }

   ngOnInit(): void {
     this.getSystemPreferences();
   }

   public getSystemPreferences(){
     try {
       this.apiservice.getSystemPreferences().subscribe(res=>{
         console.log(res);
         this.systemprefernceResponse=res.systemPreferences;
         this.catererName=this.systemprefernceResponse?.catererName;
         this.dateFormat= this.systemprefernceResponse.dateFormat==0?null:this.systemprefernceResponse.dateFormat;
          // dateFormat

       })
     } catch (error) {

     }
   }

   public saveSystemPreferences(){
    try {
      let obj={"dateFormat":this.dateFormat==null?0:this.dateFormat,"catererName":this.catererName}
      this.apiservice.saveSystemPreferences(obj).subscribe(res=>{
        console.log(res);
        Swal.fire('','Updated Successfully','success');

      })
    } catch (error) {

    }
  }


}
