import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  constructor(public apiService: ApiService) { }
  public copyRight = "";
  public literelease =
    {
      "releasedDate": "",
      "briefSummary": "",
      "windowsSupported": "",
      "deploymentDate": "",
      "resolutions": "",
      "version": ""
    }

  ngOnInit(): void {
    this.getCXPLiteAbout()
  }
  public getCXPLiteAbout() {
    try {
      this.apiService.getCXPLiteAbout().subscribe(res => {
        console.log(res)
        this.copyRight = res?.copyRight.copyRightInfo;
        this.literelease=res?.liteReleases[0];


      })
    } catch (error) {

    }
  }
}
