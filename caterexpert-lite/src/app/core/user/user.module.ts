import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { SwitchUserComponent } from './switch-user/switch-user.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { UserPrefernceComponent } from './user-prefernce/user-prefernce.component';
import { SystemPrefernceComponent } from './system-prefernce/system-prefernce.component';
import { AboutComponent } from './about/about.component';
import { SharedModule } from 'src/app/shared/shared/shared.module';
import { PrimeNgModule } from 'src/app/shared/prime-ng/prime-ng.module';


@NgModule({
  declarations: [
    SwitchUserComponent,
    ChangePasswordComponent,
    UserPrefernceComponent,
    SystemPrefernceComponent,
    AboutComponent,

  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    SharedModule,
    PrimeNgModule
  ]
})
export class UserModule { }
