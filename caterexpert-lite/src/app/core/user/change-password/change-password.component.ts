import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { DynamicDialogRef } from 'primeng/dynamicdialog';
import { ApiService } from 'src/app/service/api.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  public passwordForm: FormGroup;
  public formError: boolean = false;
  // public ShowPassword: boolean = false;
  constructor(private _fb: FormBuilder, public apiService: ApiService, public modelRef: DynamicDialogRef) { }

  ngOnInit(): void {
    this.passwordForm = this._fb.group({
      confirmPassword: new FormControl('', [Validators.required]),
      oldPassword: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),

    }, { validators: this.MatchPassword('password', 'confirmPassword') })
  }
  get form() {
    return this.passwordForm.controls;
  }

  public changePassword() {
    this.formError = true;
    if (this.passwordForm.valid) {
      let obj = { "loginId": this.apiService.loginResponse.userId, "oldPassword": this.passwordForm.value.oldPassword, "newPassword": this.passwordForm.value.oldPassword };
      try {
        this.apiService.changePassword(obj).subscribe(res => {
          console.log(res);
          if (res.flag == 1) {
            Swal.fire(`Success`, `Password Changed Successfully`, 'success');
            this.modelRef.destroy();
          } else {
            Swal.fire(``, `Failed to save Password`, 'error');
          }
        })
      } catch (error) {

      }

    }
  }
  public MatchPassword(password: string, confirmPassword: string): object {
    return (formGroup: FormGroup) => {
      const passwordControl = formGroup.controls[password];
      const confirmPasswordControl = formGroup.controls[confirmPassword];

      if (!passwordControl || !confirmPasswordControl) {
        return null;
      }

      if (confirmPasswordControl.errors && !confirmPasswordControl.errors.passwordMismatch) {
        return null;
      }

      if (passwordControl.value !== confirmPasswordControl.value) {
        confirmPasswordControl.setErrors({ passwordMismatch: true });
      } else {
        confirmPasswordControl.setErrors(null);
      }
    }
  }

}
