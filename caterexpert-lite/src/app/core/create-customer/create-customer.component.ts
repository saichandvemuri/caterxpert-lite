import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiService } from 'src/app/service/api.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-create-customer',
  templateUrl: './create-customer.component.html',
  styleUrls: ['./create-customer.component.scss']
})
export class CreateCustomerComponent implements OnInit {

  tabIndex: number;
  public customerLookup: any;
  public detailsForm: FormGroup;
  public addressForm: FormGroup;
  public billingAddressForm: FormGroup;


  constructor(public apiservice: ApiService, private _fb: FormBuilder) {
    this.apiservice.SetheaderName('Customer');
  }
  ngOnInit(): void {
    this.createForm();
    this.getCustomerLookup();
    this.tabIndex = 0;

  }
  nextPage() {
    this.tabIndex++;
  }
  prevPage() {
    this.tabIndex--;
  }
  public createForm() {
    this.detailsForm = this._fb.group({
      id:new FormControl(0),
      companyName: new FormControl(null, Validators.required),
      firstName: new FormControl('',),
      customerTypeIds: new FormControl([], Validators.required),
      billTermsId: new FormControl(null),
      businessUnitsIds: new FormControl([], Validators.required),
      referralTypeId: new FormControl(null, Validators.required),
      active: new FormControl(false)
    })
    this.addressForm = this._fb.group({
      building: new FormControl(''),
      street: new FormControl(''),
      suite: new FormControl(''),
      city: new FormControl(''),
      state: new FormControl(null, Validators.required),
      zipCode: new FormControl(null, Validators.required),
      phone: new FormControl(null, Validators.required),
      mobile: new FormControl(null, Validators.required),
      email: new FormControl(null, [Validators.required, Validators.email],),
      comments: new FormControl(null, Validators.required),
      directions: new FormControl(null, Validators.required),



    })
    this.billingAddressForm = this._fb.group({
      building: new FormControl(''),
      street: new FormControl(''),
      suite: new FormControl(''),
      city: new FormControl(''),
      state: new FormControl(null, Validators.required),
      zipCode: new FormControl(null, Validators.required),
      billContact: new FormControl(''),
      accountingId: new FormControl('')
    })
  }

  public onDetailsFormSubmit() {
    console.log(this.detailsForm.valid);
    console.log(this.detailsForm.value)
    if(this.detailsForm.valid){
      this.tabIndex++;
    }else{
      Swal.fire('','Please Fill all Mandatory Fields','warning')
    }
  }
  public onAddressFormSubmit() {
    console.log(this.addressForm.valid);
    console.log(this.addressForm.value)
  }

  get detailFormControls() {
    return this.detailsForm.controls;
  }
  public getCustomerLookup() {
    try {
      this.apiservice.getCustomerLookup().subscribe(res => {
        console.log("customer response", res)
        this.customerLookup = res.customerLookup;
      })
    } catch (error) {

    }
  }

  public saveCustomer() {
    console.log(this.billingAddressForm.value);

    let obj={
      "id": this.detailsForm.value.id,
      "companyName": this.detailsForm.value.companyName,
      "firstName": this.detailsForm.value.firstName,
      "customerTypeId": 0,
      "customerTypeIds": this.detailsForm.value.customerTypeIds.toString(),
      "addressId": "0",
      "referralTypeId": this.detailsForm.value.referralTypeId,
      "taxExempt": 1,
      "billAddressId": "0",
      "billContactName": this.billingAddressForm.value.billContact,
      "comments": this.addressForm.value.comments,
      "userId": this.apiservice.loginResponse.userId,
      "lastUpdated": "",
      "active": this.detailsForm.value.active,
      "accountingId": "",
      "createdDate": "",
      "billTermsId": 1,
      "businessUnitsId": 3,
      "businessUnitsIds": this.detailsForm.value.businessUnitsIds,
      "deliveryCharges": "15.25",
      "name": "Test Name",
      "street": this.addressForm.value.street,
      "suite": this.addressForm.value.suite,
      "city": this.addressForm.value.city,
      "stateId": this.addressForm.value.state,
      "zip": this.addressForm.value.zipCode,
      "homePhone": this.addressForm.value.mobile,
      "workPhone":this.addressForm.value.phone,
      "email": this.addressForm.value.email,
      "directions": this.addressForm.value.directions,
      "entrance": "",
      "billName": "Test Bill",
      "billStreet": this.billingAddressForm.value.street,
      "billSuite": this.billingAddressForm.value.suite,
      "billCity": this.billingAddressForm.value.city,
      "billStateId": this.billingAddressForm.value.state,
      "billZip": this.billingAddressForm.value.zipCode,
      "leadCustomerId": 0
  }
  try {
    this.apiservice.saveCustomer(obj).subscribe(res=>{
      console.log(res);
    })
  } catch (error) {

  }
  }
}
