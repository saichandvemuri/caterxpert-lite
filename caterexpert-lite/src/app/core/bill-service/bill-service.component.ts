import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/service/data.service';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-bill-service',
  templateUrl: './bill-service.component.html',
  styleUrls: ['./bill-service.component.scss']
})
export class BillServiceComponent implements OnInit {
public addBillservice:boolean = false;
public eventCredits:boolean = false;
public rejectReleasecomments:boolean = false;
public actualCost:boolean = false;
displayResponsive:boolean=false;
  customer
  constructor(private _service: DataService, private apiservice: ApiService) {
    this.data();
    this.apiservice.SetheaderName('Bill Service')
  }

  ngOnInit(): void {
  }
  data() {
    this._service.getdata().subscribe((res) => {
      console.log(res)
      this.customer = res;
    })
  }

}
