import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BillServiceComponent } from './bill-service.component';

describe('BillServiceComponent', () => {
  let component: BillServiceComponent;
  let fixture: ComponentFixture<BillServiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BillServiceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BillServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
