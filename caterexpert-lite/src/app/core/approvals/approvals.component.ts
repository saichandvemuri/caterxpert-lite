import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-approvals',
  templateUrl: './approvals.component.html',
  styleUrls: ['./approvals.component.scss']
})
export class ApprovalsComponent implements OnInit {
customer;
  constructor(private apiService:ApiService,private _service:DataService) { 
    this.apiService.SetheaderName('Approvals');
    this._service.getdata().subscribe((res)=>{
      this.customer = res;
    })
  }
  ngOnInit(): void {
  }

}
