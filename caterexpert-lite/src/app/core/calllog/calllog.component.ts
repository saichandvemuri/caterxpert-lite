import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-calllog',
  templateUrl: './calllog.component.html',
  styleUrls: ['./calllog.component.scss']
})
export class CalllogComponent implements OnInit {

  constructor(private apiService:ApiService) { 
    this.apiService.SetheaderName('Log Call')
  }

  ngOnInit(): void {
   
  }

}
