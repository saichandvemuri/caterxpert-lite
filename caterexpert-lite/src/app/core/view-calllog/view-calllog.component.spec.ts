import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewCalllogComponent } from './view-calllog.component';

describe('ViewCalllogComponent', () => {
  let component: ViewCalllogComponent;
  let fixture: ComponentFixture<ViewCalllogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewCalllogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewCalllogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
