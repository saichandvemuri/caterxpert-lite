import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-view-calllog',
  templateUrl: './view-calllog.component.html',
  styleUrls: ['./view-calllog.component.scss']
})
export class ViewCalllogComponent implements OnInit {
public cities;
constructor(private apiService:ApiService) { 
  this.apiService.SetheaderName('View Call Log')
}

  ngOnInit(): void {
    this.cities = [
      {name: 'Comnbined User, February', code: 'NY'},
      {name: 'Rome', code: 'RM'},
      {name: 'London', code: 'LDN'},
      {name: 'Istanbul', code: 'IST'},
      {name: 'Paris', code: 'PRS'},
      {name: 'Rome', code: 'RM'},
      {name: 'London', code: 'LDN'},
      {name: 'Istanbul', code: 'IST'},
      {name: 'Paris', code: 'PRS'}
  ];
  }

}
