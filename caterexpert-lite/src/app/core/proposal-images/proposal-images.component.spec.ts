import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProposalImagesComponent } from './proposal-images.component';

describe('ProposalImagesComponent', () => {
  let component: ProposalImagesComponent;
  let fixture: ComponentFixture<ProposalImagesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProposalImagesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProposalImagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
