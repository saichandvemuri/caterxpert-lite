import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-proposal-images',
  templateUrl: './proposal-images.component.html',
  styleUrls: ['./proposal-images.component.scss']
})
export class ProposalImagesComponent implements OnInit {

  customer
  constructor(private _service:DataService) { }

  ngOnInit(): void {
    this._service.getdata().subscribe((res)=>{
      this.customer = res;
    })
  }
}
