import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-finalize-menu-edit',
  templateUrl: './finalize-menu-edit.component.html',
  styleUrls: ['./finalize-menu-edit.component.scss']
})
export class FinalizeMenuEditComponent implements OnInit {
customer
  constructor(private _service:DataService) { }

  ngOnInit(): void {
    this._service.getdata().subscribe((res)=>{
      this.customer = res;
    })
  }

}
