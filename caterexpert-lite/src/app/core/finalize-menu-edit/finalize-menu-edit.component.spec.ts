import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FinalizeMenuEditComponent } from './finalize-menu-edit.component';

describe('FinalizeMenuEditComponent', () => {
  let component: FinalizeMenuEditComponent;
  let fixture: ComponentFixture<FinalizeMenuEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FinalizeMenuEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FinalizeMenuEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
