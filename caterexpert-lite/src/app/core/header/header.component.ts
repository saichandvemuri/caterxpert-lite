import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import { AboutComponent } from '../user/about/about.component';
import { ChangePasswordComponent } from '../user/change-password/change-password.component';
import {DialogService, DynamicDialogRef} from 'primeng/dynamicdialog';
import {DynamicDialogConfig} from 'primeng/dynamicdialog';
import { Router } from '@angular/router';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],

})
export class HeaderComponent implements OnInit {

  constructor(public apiService:ApiService,public modelRef: DynamicDialogRef,
     public config: DynamicDialogConfig, public modalService: DialogService,
     public router:Router) { }

  ngOnInit(): void {

  }
  // Method is used to open  change password popup
public changepassword(op):void{
  op.hide();
this.modelRef=this.modalService.open(ChangePasswordComponent,{
  header: 'Change password',
  width: '35%%',
  baseZIndex:10000

})
}

/*
 * openAbout method is used to display About component in popup
 */
public openAbout(op):void {
  op.hide();
  this.modelRef=this.modalService.open(AboutComponent,{
    width: '50%',
    baseZIndex:10000,
    header:'About',
  })
}

public logout(){
  this.apiService.loginResponse=null;
  this.router.navigateByUrl('/login');
}
}
