import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProposalPrintComponent } from './proposal-print.component';

describe('ProposalPrintComponent', () => {
  let component: ProposalPrintComponent;
  let fixture: ComponentFixture<ProposalPrintComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProposalPrintComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProposalPrintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
