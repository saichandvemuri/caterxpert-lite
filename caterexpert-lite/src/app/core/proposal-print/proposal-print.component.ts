import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-proposal-print',
  templateUrl: './proposal-print.component.html',
  styleUrls: ['./proposal-print.component.scss']
})
export class ProposalPrintComponent implements OnInit {

 
  tabIndex: number;
  cities;
  constructor(private apiservice:ApiService) {
    this.apiservice.SetheaderName('Proposal Print')
  }

  ngOnInit(): void {
    this.tabIndex=0;
    this.cities = [
      {name: 'Comnbined User, February', code: 'NY'},
      {name: 'Rome', code: 'RM'},
      {name: 'London', code: 'LDN'},
      {name: 'Istanbul', code: 'IST'},
      {name: 'Paris', code: 'PRS'},
      {name: 'Rome', code: 'RM'},
      {name: 'London', code: 'LDN'},
      {name: 'Istanbul', code: 'IST'},
      {name: 'Paris', code: 'PRS'}
  ];
  }
  nextPage(){
    this.tabIndex++;
  }
  prevPage(){
    this.tabIndex--;
  }
}
