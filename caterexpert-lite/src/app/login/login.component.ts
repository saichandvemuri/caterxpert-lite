import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from '../service/api.service';
import Swal from'sweetalert2'
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;
  public formError: boolean = false;
  public ShowPassword: boolean = false;
  constructor(private _fb: FormBuilder, private apiService: ApiService, public router: Router) {
    this.apiService.headerName = null;
    if(this.apiService.loginResponse!=null){
      this.apiService.previousRoute();
    }
  }

  ngOnInit(): void {
    this.loginForm = this._fb.group({
      catererId: new FormControl('', [Validators.required]),
      userId: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    })
  }
  get form() {
    return this.loginForm.controls;
  }
  public showpassword() {
    this.ShowPassword = !this.ShowPassword;
  }

  //  authenticateUser method to login user
  public authenticateUser() {
    console.log(this.loginForm.value);
    this.formError = true;
    if (this.loginForm.valid) {
      let obj = {
        "loginId": this.loginForm.value.userId,
        "password": this.loginForm.value.password
      }
      this.apiService.authenticateUser(obj, this.loginForm.value.catererId).subscribe(res => {
        console.log(res);
        if(res['error'].length>0){
          Swal.fire('',res.error,'error')
        }else if(res.loggedInUser==0){
          Swal.fire('','Invalid Credentials','error')
        }else{
        res.clientId=this.loginForm.value.catererId

        this.apiService.loginResponse=res;
        this.router.navigateByUrl('events/event-list')
        }
      })


    }

  }
}
