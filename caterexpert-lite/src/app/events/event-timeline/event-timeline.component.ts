import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/service/data.service';
import { SortEvent } from 'primeng/api';
import { ApiService } from 'src/app/service/api.service';
@Component({
  selector: 'app-event-timeline',
  templateUrl: './event-timeline.component.html',
  styleUrls: ['./event-timeline.component.scss']
})
export class EventTimelineComponent implements OnInit {

  public customer;
  constructor(private _service:DataService,private apiservice:ApiService) {
    this.apiservice.SetheaderName('Event Timeline')
   }
  
    ngOnInit(): void {
     
        this._service.getdata().subscribe((res) => {
          console.log(res)
          this.customer = res;
        })
  
    }
  
}
