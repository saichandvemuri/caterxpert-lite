import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-create-inquiry',
  templateUrl: './create-inquiry.component.html',
  styleUrls: ['./create-inquiry.component.scss']
})
export class CreateInquiryComponent implements OnInit {
  tabIndex: number;
  cities;
  constructor(private apiService:ApiService) {
    this.apiService.SetheaderName('Inquiry')
   }

  ngOnInit(): void {
    this.tabIndex = 0;
    this.cities = [
      { name: 'Comnbined User, February', code: 'NY' },
      { name: 'Rome', code: 'RM' },
      { name: 'London', code: 'LDN' },
      { name: 'Istanbul', code: 'IST' },
      { name: 'Paris', code: 'PRS' },
      { name: 'Rome', code: 'RM' },
      { name: 'London', code: 'LDN' },
      { name: 'Istanbul', code: 'IST' },
      { name: 'Paris', code: 'PRS' }
    ];
  }

  nextPage() {
    this.tabIndex++;
  }
  prevPage() {
    this.tabIndex--;
  }

}
