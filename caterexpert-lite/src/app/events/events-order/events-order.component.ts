import { Component, OnInit } from '@angular/core';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ApiService } from 'src/app/service/api.service';
import { DataService } from 'src/app/service/data.service';
import { EditEventOrderComponent } from '../dialog/edit-event-order/edit-event-order.component';
import { ServiceTabsComponent } from 'src/app/service-tabs/service-tabs.component';

@Component({
  selector: 'app-events-order',
  templateUrl: './events-order.component.html',
  styleUrls: ['./events-order.component.scss']
})
export class EventsOrderComponent implements OnInit {
public customer:any[];
  constructor(private _service: DataService,public apiService:ApiService,public dialogService: DialogService) {
    this.apiService.SetheaderName('Event Order');
   }

  ngOnInit(): void {
    this.data();
  }
  data() {
    this._service.getdata().subscribe((res) => {
      this.customer = res;
    })
  }
   ref: DynamicDialogRef;
//   show() {
//     this.ref = this.dialogService.open(EditEventOrderComponent, {
//         header: 'Package - Pkg All Items 2 Lunch',
//         width: '80%',
//         height: '96%',
//         contentStyle: {"max-height": "96vh", "overflow": "auto","padding-bottom":"10px"},
//         styleClass:"events-order-list",
//         baseZIndex: 10000
//     });

// //     this.ref.onClose.subscribe((product: Product) =>{
// //         if (product) {
// //             this.messageService.add({severity:'info', summary: 'Product Selected', detail: product.name});
// //         }
// //     });
// }
display: boolean = false;
show() {
  this.display = true;
}
show1() {
  this.ref = this.dialogService.open(ServiceTabsComponent, {
    styleClass:"service-tabs dynamic-dialog-container popup-cont ",
      width: '98%',
      baseZIndex: 10000
  });
}
// ngOnDestroy() {
//   if (this.ref) {
//       this.ref.close();
//   }
// }
}
