import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/service/data.service';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-event-gallery',
  templateUrl: './event-gallery.component.html',
  styleUrls: ['./event-gallery.component.scss']
})
export class EventGalleryComponent implements OnInit {
  displayResponsive: boolean = false;
  uploadedFiles: any[] = [];
  customer
  constructor(private _service:DataService,private apiservice:ApiService) {
    this.apiservice.SetheaderName('Event Gallery')
   }
  ngOnInit(): void {
this._service.getdata().subscribe((res)=>{
  this.customer =res;
})
  }
  show() {
    this.displayResponsive = true;
  }
  onUpload(event) {
    for (let file of event.files) {
      this.uploadedFiles.push(file);
    }


  }
}
