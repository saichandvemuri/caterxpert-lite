import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateEventComponent } from './create-event/create-event.component';
import { CreateInquiryComponent } from './create-inquiry/create-inquiry.component';
import { EventHistoryComponent } from './event-history/event-history.component';
import { EventListComponent } from './event-list/event-list.component';
import { EventsOrderComponent } from './events-order/events-order.component';
import { EventsComponent } from './events.component';
import { FullorderViewComponent } from './fullorder-view/fullorder-view.component';
import { RepeatEventComponent } from './repeat-event/repeat-event.component';
import { EventSetupSheetComponent } from './event-setup-sheet/event-setup-sheet.component';
import { EventTimelineComponent } from './event-timeline/event-timeline.component';
import { EventGalleryComponent } from './event-gallery/event-gallery.component';
import { CopyFromEventComponent } from './copy-from-event/copy-from-event.component';
import { SummaryCalendarViewComponent } from './summary-calendar-view/summary-calendar-view.component';

const routes: Routes = [
  {path:'',redirectTo:'/events/event-list',pathMatch:"full"},
  // { path: '', component: EventsComponent },
  { path: 'event-list', component: EventListComponent },
  { path: 'create-event', component: CreateEventComponent },
  { path: 'events-order', component: EventsOrderComponent },
  { path: 'fullorder-view', component: FullorderViewComponent },
  { path: 'inquiry', component: CreateInquiryComponent },
  { path: 'repeat-event', component: RepeatEventComponent },
  { path: "event-history", component: EventHistoryComponent },
  { path: "event-setup-sheet", component: EventSetupSheetComponent },
  { path: "event-timeline", component: EventTimelineComponent },
  { path: "event-gallery", component: EventGalleryComponent },
  {path:"copy-from-event",component:CopyFromEventComponent},
  { path: 'summaryCalenderView', component: SummaryCalendarViewComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EventsRoutingModule { }
