import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DialogService } from 'primeng/dynamicdialog';
import { Paginator } from 'primeng/paginator';

import { ApiService } from 'src/app/service/api.service';
import { DataService } from 'src/app/service/data.service';
// declare var $: any;

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.scss']
})
export class EventListComponent implements OnInit {

  public sidebarArray = [
    { id: 12, tooltip: 'eCaterXpert Orders', icon: 'rice_bowl' },
    { id: 11, tooltip: 'Approvals', icon: 'done_all' },
    { id: 10, tooltip: 'Closed Days', icon: 'event_busy' },
    { id: 9, tooltip: "View Call Log", icon: 'description' },
    { id: 8, tooltip: 'Log Call', icon: 'call' },
    { id: 7, tooltip: 'Create Inquiry', icon: 'flag' },
    { id: 6, tooltip: 'Create Event', icon: 'event' },
    { id: 5, tooltip: 'Create Customer', icon: 'person_add' },
    { id: 4, tooltip: 'Customer Search', icon: 'search' },
    { id: 3, tooltip: 'Calendar View', icon: 'event_note' },
    { id: 2, tooltip: 'Summary Calendar View', icon: 'calendar_view_month' }
  ]
  public businessUnitsList: Array<any> = [];
  public eventTypeList: Array<any> = [];
  public locationsList: Array<any> = [];
  public partyStatusList: Array<any> = [];
  public usersList: Array<any> = [];
  public eventList: Array<any> = [];
  public searchFlag: string = null;
  public eventId: string = null;
  public customerName: string = null;
  public startDate: Date = new Date();
  public endDate: Date = new Date();
  public ae: Array<any> = null;
  public eventType: number = null;
  public bussinessUnit: Array<any> = [];
  public eventStatus: Array<any> = [];
  public guestMinCount: number = null;
  public guestMaxCount: number = null;
  public location: number = null;
  public customerId: number = null;
  public contactId: number = null;
  public eventListFlag: number = null;
  public calandarViewFlag: number = null;
  public pageIndex: number = 1;
  private appliedSearchFlag: string = null;
  private appliedEventId: string = null;
  private appliedCustomerName: string = null;
  private appliedStartDate: string = null;
  private appliedEndDate: string = null;
  private appliedAe: string = null;
  private appliedEventType: number = null;
  private appliedBussinessUnit: string = null;
  private appliedEventStatus: string = null;
  private appliedGuestMinCount: any = null;
  private appliedGuestMaxCount: any = null;
  private appliedLocation: number = null;
  private appliedCustomerId: number = null;
  private appliedContactId: number = null;
  private appliedEventListFlag: number = null;
  private appliedCalandarViewFlag: number = null;
  public totalRecordCount = 0;



  constructor(private _service: DataService, private apiservice: ApiService, public datepipe: DatePipe, public dialogService: DialogService) {
    this.startDate.setDate(new Date().getDate() - 7)
    this.endDate.setDate(new Date().getDate() + 7);


    this.apiservice.SetheaderName('Event Listing', this.sidebarArray)
  }
  // rowGroupMetadata: any;
  // screenHeight
  ngOnInit(): void {
    this.getEventSearchLookUp();
    this.getFilterPreference();
    // this.getEventListWithSearch();
    // this.onFilterSearch()

  }
  public getEventSearchLookUp() {
    try {
      this.apiservice.getEventSearchLookUp().subscribe(res => {
        console.log(res);
        this.businessUnitsList = res.businessUnitsList;
        this.eventTypeList = res.eventTypeList;
        this.locationsList = res.locationsList;
        this.partyStatusList = res.partyStatusList;
        this.usersList = res.usersList;
      })
    } catch (error) {

    }
  }

  public getEventListWithSearch() {
    let obj = {
      "searchFlag": this.searchFlag,
      "eventId": this.appliedEventId,
      "customerName": this.appliedCustomerName,
      "startDate": this.appliedStartDate,
      "endDate": this.appliedEndDate,
      "ae": this.appliedAe,
      "eventType": this.appliedEventType,
      "bussinessUnit": this.appliedBussinessUnit,
      "eventStatus": this.appliedEventStatus,
      "guestCount": 0,
      "minValue": this.appliedGuestMinCount,
      "maxValue": this.appliedGuestMaxCount,
      "location": this.appliedLocation,
      "customerId": this.appliedCustomerId,
      "contactId": this.appliedContactId,
      "eventListFlag": this.appliedEventListFlag,
      "calandarViewFlag": 0,
      "pageIndex": this.pageIndex
    }
    try {
      this.apiservice.getEventListWithSearch(obj).subscribe(res => {
        console.log(res);
        this.eventList = res.eventList;
        console.log(res.customerId);
        this.totalRecordCount = this.eventList.length > 0 ? this.eventList[1].recordCount : 0;
      })
    } catch (error) {

    }
  }

  public onPageChange(event) {
    console.log(event)
    this.pageIndex = event.page;
    this.getEventListWithSearch();
  }






  display: boolean = false;
  displayResponsive: boolean = false;
  changeRequest: boolean = false;
  servicelogger: boolean = false;
  show() {
    this.display = true;
  }
  show1() {
    this.displayResponsive = true;
  }
  show2() {
    this.changeRequest = true;
  }
  show3() {
    this.servicelogger = true;
  }

  @ViewChild('p', { static: false }) paginator: Paginator;

  public onFilterSearch(event?, paginator?, filter?) {
    event && this.eventList.length > 0 ? paginator.changePageToFirst(event) : '';
    event && filter ? filter.hide() : '';
    this.pageIndex = 0;
    console.log(this.eventId)
    this.appliedEventId = this.eventId == null ? '' : this.eventId;
    this.customerName = this.customerName == null ? '' : this.customerName;
    this.appliedStartDate = this.startDate == null ? '' : this.datepipe.transform(this.startDate, 'MM/dd/yyyy');
    this.appliedEndDate = this.endDate == null ? '' : this.datepipe.transform(this.endDate, 'MM/dd/yyyy');
    this.appliedCustomerName = this.customerName == null ? '' : this.customerName;
    this.appliedAe = this.ae == null ? '' : this.ae.toString();
    this.appliedEventType = this.eventType == null ? 0 : this.eventType;
    this.appliedBussinessUnit = this.bussinessUnit == null ? '' : this.bussinessUnit.toString();
    this.appliedEventStatus = this.eventStatus == null ? '' : this.eventStatus.toString();
    this.appliedLocation = this.location == null ? 0 : this.location;
    this.appliedGuestMinCount = this.guestMinCount == null ? '' : this.guestMinCount;
    this.appliedGuestMaxCount = this.guestMaxCount == null ? '' : this.guestMaxCount;
    this.appliedContactId = this.contactId == null ? 0 : this.contactId;
    this.appliedCustomerId = this.customerId == null ? 0 : this.customerId;
    this.appliedEventListFlag = this.eventListFlag == null ? 0 : this.eventListFlag;
    this.searchFlag = this.searchFlag == null ? 'Basic' : this.searchFlag;

    this.getEventListWithSearch();




  }

  public getFilterPreference() {
    try {
      this.apiservice.getFilterPreference().subscribe(res => {
        console.log(res);
        this.startDate.setDate(new Date().getDate() - res.filterPreference.pastVal)
        this.endDate.setDate(new Date().getDate() + res.filterPreference.nextVal);

        this.eventId = res.filterPreference.eventCode == 0 ? null : res.filterPreference.eventCode;
        this.eventStatus = res.filterPreference.partyStatus.length > 0 ? res.filterPreference.partyStatus.split(',') : []
        this.bussinessUnit = res.filterPreference.businessunits.length > 0 ? res.filterPreference.businessunits.split(',') : [];
        this.ae = res.filterPreference.accountExec.length > 0 ? res.filterPreference.accountExec.split(',') : []
        this.ae = this.ae.map(x => {
          return +x
        })
        this.bussinessUnit = this.bussinessUnit.map(x => {
          return +x
        })
        this.eventStatus = this.eventStatus.map(x => {
          return +x
        })

        console.log(this.eventStatus, this.bussinessUnit, this.ae);
        this.onFilterSearch()

      })
    } catch (error) {

    }
  }

}
