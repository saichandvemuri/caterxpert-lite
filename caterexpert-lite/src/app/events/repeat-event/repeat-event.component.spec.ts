import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RepeatEventComponent } from './repeat-event.component';

describe('RepeatEventComponent', () => {
  let component: RepeatEventComponent;
  let fixture: ComponentFixture<RepeatEventComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RepeatEventComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RepeatEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
