import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-repeat-event',
  templateUrl: './repeat-event.component.html',
  styleUrls: ['./repeat-event.component.scss']
})
export class RepeatEventComponent implements OnInit {
public cities;
public customer;
  constructor( private apiservice: ApiService,private _service:DataService) {

    this.apiservice.SetheaderName('Repeat Event')
  }

  ngOnInit(): void {
    this.cities = [
      { name: 'Daily', code: 'D' },
      { name: 'Weekly', code: 'W' },
      { name: 'Monthly', code: 'M' }
  
    ];
    this.data();
  }
  data() {
    this._service.getdata().subscribe((res) => {
      console.log(res)
      this.customer = res;
    })
  }
}
