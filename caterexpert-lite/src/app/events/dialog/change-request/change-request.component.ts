import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-change-request',
  templateUrl: './change-request.component.html',
  styleUrls: ['./change-request.component.scss']
})
export class ChangeRequestComponent implements OnInit {
public customer;
public display:boolean=false;
  constructor(private _service:DataService) { }

  ngOnInit(): void {
    this.data();
  }
  data() {
    this._service.getdata().subscribe((res) => {
      console.log(res)
      this.customer = res;
    })
  }
  show(){
    this.display = true;
  }
}
