import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BatchprintComponent } from './batchprint.component';

describe('BatchprintComponent', () => {
  let component: BatchprintComponent;
  let fixture: ComponentFixture<BatchprintComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BatchprintComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BatchprintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
