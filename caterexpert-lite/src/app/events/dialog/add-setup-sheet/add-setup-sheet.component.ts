import { Component, OnInit } from '@angular/core';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ServiceTabsComponent } from 'src/app/service-tabs/service-tabs.component';

@Component({
  selector: 'app-add-setup-sheet',
  templateUrl: './add-setup-sheet.component.html',
  styleUrls: ['./add-setup-sheet.component.scss']
})
export class AddSetupSheetComponent implements OnInit {
  ref: DynamicDialogRef;
  constructor(public dialogService: DialogService) { }

  ngOnInit(): void {
  }
  showServicetabs() {
    this.ref = this.dialogService.open(ServiceTabsComponent, {
      styleClass:"service-tabs dynamic-dialog-container popup-cont ",
        width: '98%',
        baseZIndex: 10000
    });
  }
}
