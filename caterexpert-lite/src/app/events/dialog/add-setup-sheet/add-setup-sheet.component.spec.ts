import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSetupSheetComponent } from './add-setup-sheet.component';

describe('AddSetupSheetComponent', () => {
  let component: AddSetupSheetComponent;
  let fixture: ComponentFixture<AddSetupSheetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddSetupSheetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSetupSheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
