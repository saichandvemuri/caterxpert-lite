import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-estimates',
  templateUrl: './estimates.component.html',
  styleUrls: ['./estimates.component.scss']
})
export class EstimatesComponent implements OnInit {
public customer;
selectedValues;
  constructor(private _service:DataService) { }

  ngOnInit(): void {
    this.data();
  }
  data() {
    this._service.getdata().subscribe((res) => {
      this.customer = res;
    })
  }
  display: boolean = false;
  display1: boolean = false;
  show() {
    this.display = true;
  }
  showd() {
    this.display1 = true;
  }
}
