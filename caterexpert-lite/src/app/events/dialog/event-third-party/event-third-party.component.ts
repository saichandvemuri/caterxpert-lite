import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-event-third-party',
  templateUrl: './event-third-party.component.html',
  styleUrls: ['./event-third-party.component.scss']
})
export class EventThirdPartyComponent implements OnInit {
  tabIndex: number;
  public customer;
selectedValues;
  constructor(private _service:DataService) { }

  ngOnInit(): void {
    this.data();
    this.tabIndex = 0;
  }
  data() {
    this._service.getdata().subscribe((res) => {
      this.customer = res;
    })
  }
}
