import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EventThirdPartyComponent } from './event-third-party.component';

describe('EventThirdPartyComponent', () => {
  let component: EventThirdPartyComponent;
  let fixture: ComponentFixture<EventThirdPartyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EventThirdPartyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EventThirdPartyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
