import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-add-standard-items',
  templateUrl: './add-standard-items.component.html',
  styleUrls: ['./add-standard-items.component.scss']
})
export class AddStandardItemsComponent implements OnInit {
customer;
constructor(private _service:DataService) { }
  ngOnInit(): void {
    this.data();
  }
  data() {
    this._service.getdata().subscribe((res) => {
      console.log(res)
      this.customer = res;
    })
  }

}
