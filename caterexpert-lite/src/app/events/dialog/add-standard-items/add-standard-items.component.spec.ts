import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddStandardItemsComponent } from './add-standard-items.component';

describe('AddStandardItemsComponent', () => {
  let component: AddStandardItemsComponent;
  let fixture: ComponentFixture<AddStandardItemsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddStandardItemsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddStandardItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
