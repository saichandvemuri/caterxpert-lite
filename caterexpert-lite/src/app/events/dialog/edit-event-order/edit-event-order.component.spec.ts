import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditEventOrderComponent } from './edit-event-order.component';

describe('EditEventOrderComponent', () => {
  let component: EditEventOrderComponent;
  let fixture: ComponentFixture<EditEventOrderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditEventOrderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditEventOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
