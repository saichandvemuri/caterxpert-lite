import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-edit-event-order',
  templateUrl: './edit-event-order.component.html',
  styleUrls: ['./edit-event-order.component.scss']
})
export class EditEventOrderComponent implements OnInit {
public customer;
  constructor(private _service:DataService) { }

  ngOnInit(): void {
    this.data();
  }
  data() {
    this._service.getdata().subscribe((res) => {
      this.customer = res;
    })
  }
}
