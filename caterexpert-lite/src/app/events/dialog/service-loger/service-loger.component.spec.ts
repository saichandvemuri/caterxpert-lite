import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceLogerComponent } from './service-loger.component';

describe('ServiceLogerComponent', () => {
  let component: ServiceLogerComponent;
  let fixture: ComponentFixture<ServiceLogerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ServiceLogerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceLogerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
