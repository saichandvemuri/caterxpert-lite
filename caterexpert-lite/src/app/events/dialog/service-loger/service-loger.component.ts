import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-service-loger',
  templateUrl: './service-loger.component.html',
  styleUrls: ['./service-loger.component.scss']
})
export class ServiceLogerComponent implements OnInit {
public customer:any =[];
  constructor(private _service:DataService) { }

  ngOnInit(): void {
  
      this._service.getdata().subscribe((res) => {
        console.log(res)
        this.customer = res;
      })
  
  }

}
