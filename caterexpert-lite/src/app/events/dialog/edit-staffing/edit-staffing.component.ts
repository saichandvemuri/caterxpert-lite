import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/service/data.service';
declare var $:any;
@Component({
  selector: 'app-edit-staffing',
  templateUrl: './edit-staffing.component.html',
  styleUrls: ['./edit-staffing.component.scss']
})
export class EditStaffingComponent implements OnInit {
  public  color;
public  display1:boolean = false;
public customer;
public quickView:boolean =false;
public displayResponsive:boolean = false;
public customerPreference:boolean = false;
public addPreference:boolean = false;
public updateQuantity:boolean = false;
coursecomments:boolean  = false;
infomenu:boolean = false;
qviewEdit:boolean = false;
  constructor(private _service:DataService) { }

  ngOnInit(): void {
    this.sidebar();
    this._service.getdata().subscribe(res=>{
      this.customer = res;
    })
    
  }
  show(){
    this.quickView = true;
  }
show1(){
  this.displayResponsive = true;
}
show2(){
  this.customerPreference = true;
}
show3(){
  this.addPreference = true;
}
show4(){
  this.updateQuantity = true;
}
sidebar(){
  $('.ssss').click(function() {
    $('ssss').toggle("show")
});
}
}
