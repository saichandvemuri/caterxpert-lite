import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditStaffingComponent } from './edit-staffing.component';

describe('EditStaffingComponent', () => {
  let component: EditStaffingComponent;
  let fixture: ComponentFixture<EditStaffingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditStaffingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditStaffingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
