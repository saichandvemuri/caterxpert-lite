import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-additional-fees',
  templateUrl: './additional-fees.component.html',
  styleUrls: ['./additional-fees.component.scss']
})
export class AdditionalFeesComponent implements OnInit {

  public customer;
  constructor(private _service:DataService) { }
  ngOnInit(): void {
    this.data();
  }
  data() {
    this._service.getdata().subscribe((res) => {
      this.customer = res;
    })
  }
}
