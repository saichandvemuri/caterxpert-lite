import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SummaryCalendarViewComponent } from './summary-calendar-view.component';

describe('SummaryCalendarViewComponent', () => {
  let component: SummaryCalendarViewComponent;
  let fixture: ComponentFixture<SummaryCalendarViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SummaryCalendarViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SummaryCalendarViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
