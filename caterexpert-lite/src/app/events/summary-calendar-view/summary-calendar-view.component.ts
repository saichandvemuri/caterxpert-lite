import { Component, OnInit } from '@angular/core';
import { CalendarOptions } from '@fullcalendar/angular'; // useful for typechecking
import { FullCalendarComponent } from '@fullcalendar/angular';
import {  DateSelectArg, EventClickArg, EventApi } from '@fullcalendar/angular';
import { INITIAL_EVENTS, createEventId } from './event-util';





import {  ViewChild} from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
// 
@Component({
  selector: 'app-summary-calendar-view',
  templateUrl: './summary-calendar-view.component.html',
  styleUrls: ['./summary-calendar-view.component.scss']
})
export class SummaryCalendarViewComponent implements OnInit {
// references the #calendar in the template
@ViewChild('calendar') calendarComponent: FullCalendarComponent;

calendarVisible = true;
calendarOptions: CalendarOptions = {
  headerToolbar: {
    left: 'prev,today,next',
    center: 'title',
    // right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
    right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
  },
  // initialView: 'dayGridMonth',
  initialEvents: INITIAL_EVENTS, // alternatively, use the `events` setting to fetch from a feed
  weekends: true,
  editable: true,
  selectable: true,
  selectMirror: true,
  dayMaxEvents: true,
  select: this.handleDateSelect.bind(this),
  eventClick: this.handleEventClick.bind(this),
  eventsSet: this.handleEvents.bind(this),
  dateClick: this.handleDateClick.bind(this) // bind is important!
  /* you can update a remote database when these fire:
  eventAdd:
  eventChange:
  eventRemove:
  */
};
currentEvents: EventApi[] = [];
handleDateClick(arg) {
  alert('date click! ' + arg.dateStr)
}
handleCalendarToggle() {
  this.calendarVisible = !this.calendarVisible;
}
handleWeekendsToggle() {
  const { calendarOptions } = this;
  calendarOptions.weekends = !calendarOptions.weekends;
}
handleDateSelect(selectInfo: DateSelectArg) {
  const title = prompt('Please enter a new title for your event');
  const calendarApi = selectInfo.view.calendar;

  calendarApi.unselect(); // clear date selection

  if (title) {
    calendarApi.addEvent({
      id: createEventId(),
      title,
      start: selectInfo.startStr,
      end: selectInfo.endStr,
      allDay: selectInfo.allDay
    });
  }
}
handleEventClick(clickInfo: EventClickArg) {
  if (confirm(`Are you sure you want to delete the event '${clickInfo.event.title}'`)) {
    clickInfo.event.remove();
  }
}
handleEvents(events: EventApi[]) {
  this.currentEvents = events;
}
constructor( private apiservice: ApiService) {
  this.apiservice.SetheaderName('Summary Calendar View')
}
  ngOnInit() {
  }
}
