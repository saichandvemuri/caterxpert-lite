import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-copy-from-event',
  templateUrl: './copy-from-event.component.html',
  styleUrls: ['./copy-from-event.component.scss']
})
export class CopyFromEventComponent implements OnInit {
customer;
  constructor(private _service:DataService,private apiservice:ApiService) { 
    this.apiservice.SetheaderName('Copy From Event')
  }

  ngOnInit(): void {
    this.data();
  }
  data() {
    this._service.getdata().subscribe((res) => {
      console.log(res)
      this.customer = res;
      // console.log(this.customer)
    })
  }
}
