import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CopyFromEventComponent } from './copy-from-event.component';

describe('CopyFromEventComponent', () => {
  let component: CopyFromEventComponent;
  let fixture: ComponentFixture<CopyFromEventComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CopyFromEventComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CopyFromEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
