import { Component, OnInit } from '@angular/core';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ServiceTabsComponent } from 'src/app/service-tabs/service-tabs.component';
import { ApiService } from 'src/app/service/api.service';
import { DataService } from 'src/app/service/data.service';
declare var $:any;
@Component({
  selector: 'app-fullorder-view',
  templateUrl: './fullorder-view.component.html',
  styleUrls: ['./fullorder-view.component.scss']
})
export class FullorderViewComponent implements OnInit {
  cities;
  public editStaffing:boolean = false;
  public quickView:boolean = false;
  public displayResponsive1:boolean = false;
  coursecomments:boolean = false;
  customer;
  ref: DynamicDialogRef;
  constructor(public apiService:ApiService,public _service:DataService,public dialogService: DialogService) {
    this.apiService.SetheaderName('Full Order View')
   }

  ngOnInit(): void {
    this.cities = [
      {name: 'Comnbined User, February', code: 'NY'},
      {name: 'Rome', code: 'RM'},
      {name: 'London', code: 'LDN'},
      {name: 'Istanbul', code: 'IST'},
      {name: 'Paris', code: 'PRS'},
      {name: 'Rome', code: 'RM'},
      {name: 'London', code: 'LDN'},
      {name: 'Istanbul', code: 'IST'},
      {name: 'Paris', code: 'PRS'}
  ];
  this._service.getdata().subscribe(res=>{
    this.customer = res;
  })
  this.sidebar();
  }
show(){
  this.editStaffing = true;
}
sidebar(){
  $('.ssss').click(function() {
    $('ssss').toggle("show")
});
}
show1() {
  this.ref = this.dialogService.open(ServiceTabsComponent, {
    styleClass:"service-tabs dynamic-dialog-container popup-cont ",
      width: '98%',
      baseZIndex: 10000
  });
}
}
