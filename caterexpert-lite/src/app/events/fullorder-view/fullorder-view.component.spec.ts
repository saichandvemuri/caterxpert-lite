import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FullorderViewComponent } from './fullorder-view.component';

describe('FullorderViewComponent', () => {
  let component: FullorderViewComponent;
  let fixture: ComponentFixture<FullorderViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FullorderViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FullorderViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
