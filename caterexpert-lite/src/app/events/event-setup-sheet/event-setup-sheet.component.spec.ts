import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EventSetupSheetComponent } from './event-setup-sheet.component';

describe('EventSetupSheetComponent', () => {
  let component: EventSetupSheetComponent;
  let fixture: ComponentFixture<EventSetupSheetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EventSetupSheetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EventSetupSheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
