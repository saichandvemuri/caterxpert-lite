import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/service/data.service';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-event-setup-sheet',
  templateUrl: './event-setup-sheet.component.html',
  styleUrls: ['./event-setup-sheet.component.scss']
})
export class EventSetupSheetComponent implements OnInit {
public customer;
public setupAdd;
  constructor(private _service:DataService,private apiservice:ApiService) {
    this.apiservice.SetheaderName('Event Setup Sheet')
   }
  ngOnInit(): void {
      this._service.getdata().subscribe((res) => {
        console.log(res)
        this.customer = res;
      })
  }
show(){
  this.setupAdd = true;
}
}
