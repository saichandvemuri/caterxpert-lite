import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EventsRoutingModule } from './events-routing.module';
import { EventsComponent } from './events.component';
import { EventsOrderComponent } from './events-order/events-order.component';
import { PrimeNgModule } from '../shared/prime-ng/prime-ng.module';
import { FullorderViewComponent } from './fullorder-view/fullorder-view.component';
import { CreateInquiryComponent } from './create-inquiry/create-inquiry.component';
import { EditEventOrderComponent } from './dialog/edit-event-order/edit-event-order.component';
import { EstimatesComponent } from './dialog/estimates/estimates.component';
import { EventListComponent } from './event-list/event-list.component';
import { CreateEventComponent } from './create-event/create-event.component';
import { EventThirdPartyComponent } from './dialog/event-third-party/event-third-party.component';
import { AdditionalFeesComponent } from './dialog/additional-fees/additional-fees.component';
import { RepeatEventComponent } from './repeat-event/repeat-event.component';
import { EventHistoryComponent } from './event-history/event-history.component';
import { ReportsComponent } from './dialog/reports/reports.component';
import { ChangeRequestComponent } from './dialog/change-request/change-request.component';
import { AddChangeRequestComponent } from './dialog/add-change-request/add-change-request.component';
import { ServiceLogerComponent } from './dialog/service-loger/service-loger.component';
import { EventSetupSheetComponent } from './event-setup-sheet/event-setup-sheet.component';
import { AddSetupSheetComponent } from './dialog/add-setup-sheet/add-setup-sheet.component';
import { EventTimelineComponent } from './event-timeline/event-timeline.component';
import { EventGalleryComponent } from './event-gallery/event-gallery.component';
import { SharedModule } from '../shared/shared/shared.module';
import { EditStaffingComponent } from './dialog/edit-staffing/edit-staffing.component';
import { CopyFromEventComponent } from './copy-from-event/copy-from-event.component';
import { AddStandardItemsComponent } from './dialog/add-standard-items/add-standard-items.component';
import { BatchprintComponent } from './dialog/batchprint/batchprint.component';
import { ServiceInfoComponent } from './dialog/service-info/service-info.component';
import { SummaryCalendarViewComponent } from './summary-calendar-view/summary-calendar-view.component';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { FullCalendarModule } from '@fullcalendar/angular'; // must go before plugins
import dayGridPlugin from '@fullcalendar/daygrid'; // a plugin!
import timeGridPlugin from '@fullcalendar/timegrid'; // a plugin!
import listPlugin from '@fullcalendar/list';
import interactionPlugin from '@fullcalendar/interaction'; // a plugin!
FullCalendarModule.registerPlugins([ // register FullCalendar plugins
  dayGridPlugin,
  timeGridPlugin,
  listPlugin,
  interactionPlugin
]);


@NgModule({
  declarations: [
    EventsComponent,
    EventsOrderComponent,
    FullorderViewComponent,
    CreateInquiryComponent,
    EditEventOrderComponent,
    EstimatesComponent,
    EventListComponent,
    CreateEventComponent,
    EventThirdPartyComponent,
    AdditionalFeesComponent,
    RepeatEventComponent,
    EventHistoryComponent,
    ReportsComponent,
    ChangeRequestComponent,
    AddChangeRequestComponent,
    ServiceLogerComponent,
    EventSetupSheetComponent,
    AddSetupSheetComponent,
    EventTimelineComponent,
    EventGalleryComponent,
    EditStaffingComponent,
    CopyFromEventComponent,
    AddStandardItemsComponent,
    BatchprintComponent,
    ServiceInfoComponent,
    SummaryCalendarViewComponent,

  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    EventsRoutingModule,
    PrimeNgModule,
    SharedModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    }),
    FullCalendarModule // register FullCalendar with you app
  ],
  // providers:[DataService,ApiService,DatePipe]


})
export class EventsModule { }
