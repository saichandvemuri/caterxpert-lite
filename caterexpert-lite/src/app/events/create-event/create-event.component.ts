import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiService } from 'src/app/service/api.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.scss']
})
export class CreateEventComponent implements OnInit {

  public sidebarArray = [
    { id: 7, tooltip: 'Create Inquiry', icon: 'flag' },
    { id: 3, tooltip: 'Calendar View', icon: 'event_note' },
    { id: 1, tooltip: 'Event List', icon: 'format_list_bulleted' },
    { id: 4, tooltip: 'Customer Search', icon: 'search' },

  ]
  tabIndex: number = 0;

  public customerId: number = 0;
  public eventid: number = 0;
  public cisNumber: number = 0;
  public detailsForm: FormGroup;
  public contactInformationForm: FormGroup;
  public deliveryInformation: FormGroup;
  public lookups: any;
  public formError: boolean = false;
  public dateFormat = "";
  public getEventLookupResponse: any;
  public filteredCustomer = []
  public defPickupTime: number = 0;
  public defDelTime: number = 0;
  constructor(public apiservice: ApiService, public fb: FormBuilder, public datepipe: DatePipe) {
    this.apiservice.SetheaderName('Event')
  }
  ngOnInit(): void {
    this.createForms();
    this.getEventLookups();

    console.log("DATE TEST", new Date().getSeconds(), new Date())

// this.displayResponse= true;
  }
  public createForms() {
    this.detailsForm = this.fb.group({
      customerId: new FormControl(null, Validators.required),
      ae: new FormControl(null, Validators.required),
      beginTime: new FormControl(new Date(), Validators.required),
      endTime: new FormControl(new Date(), Validators.required),
      eventType: new FormControl(null, Validators.required),
      status: new FormControl(null, Validators.required),
      eventName: new FormControl(null, Validators.required),
      billCustomer: new FormControl(null, Validators.required),
      paltterType: new FormControl(null),
      bussinessUnit: new FormControl(null,),
      guestCount: new FormControl(null, Validators.required),
      eventTheme: new FormControl(null)

    })
    this.contactInformationForm = this.fb.group({
      locationName: new FormControl('',),
      street: new FormControl('', Validators.required),
      suite: new FormControl(''),
      city: new FormControl('', Validators.required),
      country: new FormControl(null, Validators.required),
      state: new FormControl(null,Validators.required),
      zip: new FormControl('', Validators.required),
      phone: new FormControl('',),
      entrance: new FormControl(''),
      contactName: new FormControl(null,Validators.required),
      contactMobile: new FormControl(null, Validators.required),
      contactPhone: new FormControl(null, Validators.required),
      contactEmail: new FormControl(null, Validators.required)
    });


  }
  get detailform() {
    return this.detailsForm.controls;
  }


  public getEventLookups() {
    try {
      let obj = { "customerId": this.customerId, "eventId": this.eventid };

      this.apiservice.getEventLookups(obj).subscribe(res => {
        console.log(res[1])
        this.getEventLookupResponse = res;
        this.defDelTime = this.getEventLookupResponse.systemPreferences.defDelTime;
        this.defPickupTime = this.getEventLookupResponse.systemPreferences.defPickupTime;
        this.lookups = res.eventLookups;
        res.systemPreferences.liteDateformat.map((x) => {
          if (x.id == res.systemPreferences.dateFormat) {
            this.dateFormat = x.mvcDateformat
          }
        })
      })

    } catch (error) {

    }
  }
  public submitDetails() {
    console.log(this.detailsForm.value)
    if (this.detailsForm.valid) {
      this.tabIndex++;
      this.formError = true;
    } else {
      this.formError = false;
      Swal.fire('', 'Please fill the mandatory fields to proceed Next', 'error')
    }
  }
  private foodDeliveryBeginTime = '';
  private foodDeliveryEndTime = '';
  private foodPickupBeginTime = '';
  private foodPickupEndtime = '';
  private eqpDeliveryBeginTime = '';
  private eqpDeliveryEndTime = ''
  private equpPickupBeginTime = '';
  private equpPickupEndtime = '';


  public submitContactDetails() {
    console.log(this.contactInformationForm.value.contactPhone,this.contactInformationForm.value)
    if (this.contactInformationForm.valid) {
      this.formError = true;
        this.deliveryInformation = this.fb.group({
        foodDeliveryBeginTime: new FormControl(new Date(Date.parse(this.detailsForm.value.beginTime) - (this.defDelTime * 60000))),
        foodDeliveryEnd: new FormControl(new Date(Date.parse(this.detailsForm.value.beginTime) - (this.defDelTime * 60000))),
        foodDeliveryCheck: new FormControl(false),
        foodPickUpBeginTime: new FormControl(new Date(Date.parse(this.detailsForm.value.endTime ) + (this.defPickupTime * 60000))),
        foodPickUpEnd: new FormControl(new Date(Date.parse(this.detailsForm.value.endTime ) + (this.defPickupTime * 60000))),
        foodPickupCheckup: new FormControl(false),
        equipDeliverBeginTime: new FormControl(new Date(Date.parse(this.detailsForm.value.beginTime) - (this.defDelTime * 60000))),
        equipDeliverEndTime: new FormControl(new Date(Date.parse(this.detailsForm.value.beginTime) - (this.defDelTime * 60000))),
        equipDeliverCheckup: new FormControl(false),
        equipPickupCheckup: new FormControl(false),
        equipPickupBeginTime: new FormControl(new Date(Date.parse(this.detailsForm.value.endTime) + (this.defPickupTime * 60000))),
        equipPickupEndTime: new FormControl(new Date(Date.parse(this.detailsForm.value.endTime) + (this.defPickupTime * 60000))),
        comments: new FormControl(''),
        deliveryCharges:new FormControl(null,Validators.required),

      })
      console.log(this.defPickupTime)

      this.tabIndex = 2;
    } else {
      this.formError = false;
      Swal.fire('', 'Please fill the mandatory fields to proceed Next', 'error')
    }
  }
  public saveDelivery() {
    if (this.deliveryInformation.valid) {

      this.foodDeliveryBeginTime = this.datepipe.transform(this.deliveryInformation.value.foodDeliveryBeginTime, 'MM/dd/yyyy') + ' ' + this.datepipe.transform(this.deliveryInformation.value.foodDeliveryBeginTime, 'hh:mm a');
      this.foodDeliveryEndTime = this.datepipe.transform(this.deliveryInformation.value.foodDeliveryBeginTime, 'MM/dd/yyyy') + ' ' + this.datepipe.transform(this.deliveryInformation.value.foodDeliveryEnd, 'hh:mm a');
      this.foodPickupBeginTime = this.datepipe.transform(this.deliveryInformation.value.foodPickUpBeginTime, 'MM/dd/yyyy') + ' ' + this.datepipe.transform(this.deliveryInformation.value.foodPickUpBeginTime, 'hh:mm a');
      this.foodPickupEndtime = this.datepipe.transform(this.deliveryInformation.value.foodPickUpBeginTime, 'MM/dd/yyyy') + ' ' + this.datepipe.transform(this.deliveryInformation.value.foodPickUpEnd, 'hh:mm a');
      this.eqpDeliveryBeginTime = this.datepipe.transform(this.deliveryInformation.value.equipDeliverBeginTime, 'MM/dd/yyyy') + ' ' + this.datepipe.transform(this.deliveryInformation.value.equipDeliverBeginTime, 'hh:mm a');
      this.eqpDeliveryEndTime = this.datepipe.transform(this.deliveryInformation.value.equipDeliverBeginTime, 'MM/dd/yyyy') + ' ' + this.datepipe.transform(this.deliveryInformation.value.equipDeliverEndTime, 'hh:mm a');
      this.equpPickupBeginTime = this.datepipe.transform(this.deliveryInformation.value.equipPickupBeginTime, 'MM/dd/yyyy') + ' ' + this.datepipe.transform(this.deliveryInformation.value.equipPickupBeginTime, 'hh:mm a');
      this.equpPickupEndtime = this.datepipe.transform(this.deliveryInformation.value.equipPickupBeginTime, 'MM/dd/yyyy') + ' ' + this.datepipe.transform(this.deliveryInformation.value.equipPickupEndTime, 'hh:mm a');


      console.log("Food delivery begin time", this.foodDeliveryBeginTime);
      this.SaveEvent();

    } else {
      Swal.fire('', 'Please fill the mandatory fields to proceed Next', 'error');
    }
  }

  public SaveEvent() {
    console.log(this.contactInformationForm['contactphone'])
    let obj2={
      "id":this.eventid,
      "cisNumber":this.cisNumber,
      "customerId":this.detailsForm.value.customerId.id,
      "eventTypeId": this.detailsForm.value.eventType,
      "eventDate":this.datepipe.transform(this.detailsForm.value.beginTime, 'MM/dd/yyyy'),
      "beginTime":this.datepipe.transform(this.detailsForm.value.beginTime, 'MM/dd/yyyy hh:mm a'),
      "endTime":this.datepipe.transform(this.detailsForm.value.endTime, 'MM/dd/yyyy hh:mm a'),
      "deliveryTime":this.foodDeliveryBeginTime,
      "pickupTime":this.foodPickupBeginTime,
      "guestCount":this.detailsForm.value.guestCount,
      "locationId":0,
      "street": this.contactInformationForm.value.street,
      "suite": this.contactInformationForm.value.suite,
      "city": this.contactInformationForm.value.city,
      "stateId": this.contactInformationForm.value.state,
      "zip": this.contactInformationForm.value.zip,
      "entrance": this.contactInformationForm.value.entrance,
      "partyStatusId": this.detailsForm.value.status,
      "customerContactId":169,
      "contactWorkPhone":this.contactInformationForm.value.contactMobile,
      "contactHomePhone":this.contactInformationForm.value.contactPhone,
      "contactFax":"",
      "otherGuests":"0",
      "comments":this.deliveryInformation.value.comments,
      "salesExecutiveId":this.detailsForm.value.ae,
      "equipDeliveryTime":this.eqpDeliveryBeginTime,
      "equipPickupTime":this.equpPickupBeginTime,
      "userId":this.apiservice.loginResponse.loggedInUser,
      "lastUpdated":"",
      "eventTypeFlag":"",
      "otherLocation":this.contactInformationForm.value.locationName,
      "eventContact":this.contactInformationForm.value.contactName,
      "perPersonCost":0,
      "inquiryDate":"",
      "eventName": this.detailsForm.value.eventName,
      "customerEmail":this.contactInformationForm.value.contactEmail,
      "deliveryEndTime":this.foodDeliveryEndTime,
      "pickupEndTime":this.foodPickupEndtime,
      "termId":0,
      "depositAmount":0,
      "purchaseOrder":"",
      "discount":0,
      "deliveryTypeId":0,
      "deliveryCharges":this.deliveryInformation.value.deliveryCharges,
      "discountType":"%",
      "clientInfo":"",
      "locationContact":"",
      "locationPhone":this.contactInformationForm.value.phone,
      "staffGuests":0,
      "bandGuests":0,
      "miscGuests":0,
      "cancelComments":"",
      "countyId":this.contactInformationForm.value.country,
      "firstCallDate":"",
      "proposalStatusId":1,
      "eventThemeId": this.detailsForm.value.eventTheme,
      "billCustomerId": this.detailsForm.value.billCustomer,
      "equipDeliveryEndTime":this.eqpDeliveryEndTime,
      "equipPickupEndTime":this.equpPickupEndtime,
      "businessUnitsId":this.detailsForm.value.bussinessUnit,
      "corpFlag":"0",
      "eventOriginalStatusId":0,
      "tentativeStatusDate":"",
      "definiteStatusDate":"",
      "lostStatusDate":"",
      "leadId":"0",
      "leadEventConvertFlag":0,
      "foodDeliveryChk": this.deliveryInformation.value.foodDeliveryEnd ? 1 : 0,
      "foodPickupChk": this.deliveryInformation.value.foodPickupCheckup ? 1 : 0,
      "equipmentDeliveryChk": this.deliveryInformation.value.equipDeliverCheckup ? 1 : 0,
      "equipmentPickupChk": this.deliveryInformation.value.equipPickupCheckup ? 1 : 0,
      "constraintIds":"",
      "constraintComments":this.deliveryInformation.value.comments,
      "eventIdTasting":0,
      "platterTypeId":this.detailsForm.value.paltterType,
     "revenueStreamId":0.
     }
console.log(obj2)
console.log(JSON.stringify(obj2))
    try {
      this.apiservice.saveEvent(obj2).subscribe(res => {
        console.log(res)
        alert(JSON.stringify(res))
        this.eventResponse=res;
        this.displayResponse=true;

      })
    } catch (error) {

    }
  }
  public eventResponse:any;
  public displayResponse=false;
  public addNewCustomer() {
    console.log(typeof (this.detailsForm.value.customerId), this.detailsForm.value.customerId)

    Swal.fire({
      title: 'Are you Sure?',
      text: 'Do you want to create New Customer',
      showDenyButton: true,
      // showCancelButton: true,
      denyButtonText: `No`,
      confirmButtonText: `Yes`,

    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        // Swal.fire('Saved!', '', 'success')
      } else if (result.isDenied) {
        // Swal.fire('Changes are not saved', '', 'info')
      }
    })
  }
  nextPage() {
    this.tabIndex++;
  }
  prevPage() {
    this.tabIndex--;
  }
  public filterCustomer(event) {
    //in a real application, make a request to a remote url with the query and return filtered results, for demo we filter at client side
    let filtered: any[] = [];
    let query = event.query;
    for (let i = 0; i < this.lookups?.customersList.length; i++) {
      let country = this.lookups?.customersList[i];
      if (country.name.toLowerCase().indexOf(query.toLowerCase()) == 0) {
        filtered.push(country);
      }
    }

    this.filteredCustomer = filtered;
  }

  publci


}
