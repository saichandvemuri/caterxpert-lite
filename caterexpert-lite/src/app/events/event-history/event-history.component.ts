import { Component, OnInit } from '@angular/core';
import { DialogService } from 'primeng/dynamicdialog';
import { ApiService } from 'src/app/service/api.service';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-event-history',
  templateUrl: './event-history.component.html',
  styleUrls: ['./event-history.component.scss']
})
export class EventHistoryComponent implements OnInit {
  customer
  constructor(private _service: DataService, private apiservice: ApiService,public dialogService: DialogService) {
    this.data();
    this.apiservice.SetheaderName('Event History')
  }

  ngOnInit(): void {
  }
  data() {
    this._service.getdata().subscribe((res) => {
      console.log(res)
      this.customer = res;
    })
  }
}
