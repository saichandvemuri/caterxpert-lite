import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataService } from 'src/app/service/data.service';
import { ApiService } from 'src/app/service/api.service';
// import { ServiceTabsComponent } from 'src/app/service-tabs/service-tabs.component';





@NgModule({
  declarations: [  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule

  ],
  exports:[
  FormsModule,
  ReactiveFormsModule],
  // providers:[ApiService,DatePipe]

})
export class SharedModule { }
