import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-ce-customer',
  templateUrl: './ce-customer.component.html',
  styleUrls: ['./ce-customer.component.scss']
})
export class CeCustomerComponent implements OnInit {
  public sidebarArray = [
    { id: 8, tooltip: 'Log Call', icon: 'call' },
    { id: 6, tooltip: 'Create Event', icon: 'event' },
    { id: 7, tooltip: 'Create Inquiry', icon: 'flag' },
    { id: 15, tooltip: 'Create Contact', icon: 'flag' },
    { id: 3, tooltip: 'Calendar View', icon: 'event_note' },
    { id: 1, tooltip: 'Event List', icon: 'format_list_bulleted' },
    { id: 4, tooltip: 'Customer Search', icon: 'search' },

  ]

  tabIndex: number;
  cities;
  constructor(public apiService: ApiService) {
    console.log("customer");
    this.apiService.SetheaderName('Customer',this.sidebarArray)

  }

  ngOnInit(): void {
    this.tabIndex = 0;
    this.cities = [
      { name: 'Comnbined User, February', code: 'NY' },
      { name: 'Rome', code: 'RM' },
      { name: 'London', code: 'LDN' },
      { name: 'Istanbul', code: 'IST' },
      { name: 'Paris', code: 'PRS' },
      { name: 'Rome', code: 'RM' },
      { name: 'London', code: 'LDN' },
      { name: 'Istanbul', code: 'IST' },
      { name: 'Paris', code: 'PRS' }
    ];
  }
  nextPage() {
    this.tabIndex++;
  }
  prevPage() {
    this.tabIndex--;
  }
}
