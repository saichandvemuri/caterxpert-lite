import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CeCustomerComponent } from './ce-customer.component';

describe('CeCustomerComponent', () => {
  let component: CeCustomerComponent;
  let fixture: ComponentFixture<CeCustomerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CeCustomerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CeCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
