import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxExemptComponent } from './tax-exempt.component';

describe('TaxExemptComponent', () => {
  let component: TaxExemptComponent;
  let fixture: ComponentFixture<TaxExemptComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TaxExemptComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxExemptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
