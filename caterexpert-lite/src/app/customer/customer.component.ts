import { Component, OnInit } from '@angular/core';
import { ApiService } from '../service/api.service';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss']
})
export class CustomerComponent implements OnInit {
   public currentindex:number=0;
  constructor(private apiservice:ApiService) {
    this.apiservice.SetheaderName('Customer')
   }
  ngOnInit(): void {
  }

   /**
   * onTabChange Method is used to activate corrosponding component
   */
  public onTabChange(event) {
    this.currentindex=event.index;

  }

}
