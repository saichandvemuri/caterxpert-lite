import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-preferences',
  templateUrl: './preferences.component.html',
  styleUrls: ['./preferences.component.scss']
})
export class PreferencesComponent implements OnInit {

  public customer:any;
  public displayResponsive: boolean;
    constructor(private _service:DataService) {
      this.data();
     }

    ngOnInit(): void {
    }
    data(){
      this._service.getdata().subscribe((res)=>{
        console.log(res)
        this.customer = res;
        // console.log(this.customer)
      })
    }
    showResponsiveDialog(){
      this.displayResponsive = true;
    }
}
