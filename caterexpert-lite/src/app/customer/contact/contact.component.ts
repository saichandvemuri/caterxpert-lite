import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  public customer:any;
    constructor(private _service:DataService) {
      this.data();
     }

    ngOnInit(): void {
    }
    data(){
      this._service.getdata().subscribe((res)=>{
        console.log(res)
        this.customer = res;
        // console.log(this.customer)
      })
    }
}
