import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-ce-contact',
  templateUrl: './ce-contact.component.html',
  styleUrls: ['./ce-contact.component.scss']
})
export class CeContactComponent implements OnInit {


  tabIndex: number;
  cities;
  constructor(private apiservice:ApiService) {
    this.apiservice.SetheaderName('Contact')
  }

  ngOnInit(): void {
    this.tabIndex=0;
    this.cities = [
      {name: 'Comnbined User, February', code: 'NY'},
      {name: 'Rome', code: 'RM'},
      {name: 'London', code: 'LDN'},
      {name: 'Istanbul', code: 'IST'},
      {name: 'Paris', code: 'PRS'},
      {name: 'Rome', code: 'RM'},
      {name: 'London', code: 'LDN'},
      {name: 'Istanbul', code: 'IST'},
      {name: 'Paris', code: 'PRS'}
  ];
  }
  nextPage(){
    this.tabIndex++;
  }
  prevPage(){
    this.tabIndex--;
  }
}
