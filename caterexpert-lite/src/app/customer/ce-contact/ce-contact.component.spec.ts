import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CeContactComponent } from './ce-contact.component';

describe('CeContactComponent', () => {
  let component: CeContactComponent;
  let fixture: ComponentFixture<CeContactComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CeContactComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CeContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
