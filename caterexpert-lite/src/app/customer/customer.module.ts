import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrimeNgModule } from '../shared/prime-ng/prime-ng.module';
import { CustomerRoutingModule } from './customer-routing.module';
import { CustomerComponent } from './customer.component';
import { ContactComponent } from './contact/contact.component';
import { EventComponent } from './event/event.component';
import { TaxExemptComponent } from './tax-exempt/tax-exempt.component';
import { PreferencesComponent } from './preferences/preferences.component';
import { NotesComponent } from './notes/notes.component';
import { TasksComponent } from './tasks/tasks.component';
import { CeCustomerComponent } from './ce-customer/ce-customer.component';
import { CeContactComponent } from './ce-contact/ce-contact.component';
import { CustomerSearchComponent } from './customer-search/customer-search.component';



@NgModule({
  declarations: [
    CustomerComponent,
    ContactComponent,
    EventComponent,
    TaxExemptComponent,
    PreferencesComponent,
    NotesComponent,
    TasksComponent,
    CeCustomerComponent,
    CeContactComponent,
    CustomerSearchComponent
  ],
  imports: [
    CommonModule,
    CustomerRoutingModule,
    PrimeNgModule
  ]
})
export class CustomerModule { }
