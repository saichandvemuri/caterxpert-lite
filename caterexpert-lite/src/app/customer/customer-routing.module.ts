import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CeContactComponent } from './ce-contact/ce-contact.component';
import { CeCustomerComponent } from './ce-customer/ce-customer.component';
import { CustomerSearchComponent } from './customer-search/customer-search.component';
import { CustomerComponent } from './customer.component';

const routes: Routes = [
{ path: '', redirectTo:'/customer/customersearch',pathMatch:'full' },
{ path: 'customersearch', component: CustomerSearchComponent },
{path:"customertabs",component:CustomerComponent},
{path:"createcontact",component:CeContactComponent}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerRoutingModule { }
