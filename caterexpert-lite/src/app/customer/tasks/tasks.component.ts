import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit {

  public customer:any;
  public displayResponsive: boolean;
    constructor(private _service:DataService) {
      this.data();
     }

    ngOnInit(): void {
    }
    data(){
      this._service.getdata().subscribe((res)=>{
        console.log(res)
        this.customer = res;
        // console.log(this.customer)
      })
    }
    showResponsiveDialog(){
      this.displayResponsive = true;
    }
}


