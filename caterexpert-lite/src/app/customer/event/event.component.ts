import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss']
})
export class EventComponent implements OnInit {

  public customer:any;
  constructor(private _service:DataService) {
    console.log("CustomerEvent")
    this.data();
   }

  ngOnInit(): void {
  }
  data(){
    this._service.getdata().subscribe((res)=>{
      console.log(res)
      this.customer = res;
      // // console.log(this.customer)
    })
  }

}
