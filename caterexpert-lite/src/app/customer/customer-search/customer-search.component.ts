import { Component, OnInit } from '@angular/core';
import { DynamicDialogRef, DynamicDialogConfig, DialogService } from 'primeng/dynamicdialog';
import { ApiService } from 'src/app/service/api.service';
import { DataService } from 'src/app/service/data.service';
import { EventComponent } from '../event/event.component';

@Component({
  selector: 'app-customer-search',
  templateUrl: './customer-search.component.html',
  styleUrls: ['./customer-search.component.scss']
})
export class CustomerSearchComponent implements OnInit {
  public sidebarArray = [
    { id: 11, tooltip: 'Approvals', icon: 'done_all' },
    { id: 10, tooltip: 'Closed Days', icon: 'event_busy' },
    { id: 9, tooltip: "View Call Log", icon: 'description' },
    { id: 8, tooltip: 'Log Call', icon: 'call' },
    { id: 7, tooltip: 'Create Inquiry', icon: 'flag' },
    { id: 4, tooltip: 'Customer Search', icon: 'search' },
    { id: 6, tooltip: 'Create Event', icon: 'event' },
    { id: 5, tooltip: 'Create Customer', icon: 'person_add' },
    { id: 1, tooltip: 'Event List', icon: 'format_list_bulleted' },
    { id: 3, tooltip: 'Calendar View', icon: 'event_note' },
  ]

  public customer: any;
  selectedProduct1;
  cities
  constructor(private _service: DataService, public apiService: ApiService, public modelRef: DynamicDialogRef, public config: DynamicDialogConfig, public modalService: DialogService) {

    this.data();
    this.apiService.SetheaderName('Customer Search', this.sidebarArray)
  }
  rowGroupMetadata: any;
  screenHeight
  ngOnInit(): void {
    this.cities = [
      { name: 'Comnbined User, February', code: 'NY' },
      { name: 'Rome', code: 'RM' },
      { name: 'London', code: 'LDN' },
      { name: 'Istanbul', code: 'IST' },
      { name: 'Paris', code: 'PRS' },
      { name: 'Rome', code: 'RM' },
      { name: 'London', code: 'LDN' },
      { name: 'Istanbul', code: 'IST' },
      { name: 'Paris', code: 'PRS' }
    ];
  }
  data() {
    this._service.getdata().subscribe((res) => {
      console.log(res)
      this.customer = res;
      // console.log(this.customer)
    })
  }

  /**
   * viewEvents method to display events in popUp
   */
  public viewEvents(op) {
    op.hide();
    this.modelRef = this.modalService.open(EventComponent, {
      // width: '35%%',
      baseZIndex: 10000,
      showHeader: false,
      dismissableMask: true
    })
  }
}
