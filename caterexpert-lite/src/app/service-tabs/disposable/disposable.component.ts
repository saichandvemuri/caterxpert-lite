import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-disposable',
  templateUrl: './disposable.component.html',
  styleUrls: ['./disposable.component.scss']
})
export class DisposableComponent implements OnInit {

  customer;
  cities;
    constructor(private _service:DataService) { }
  
    ngOnInit(): void {
      this._service.getdata().subscribe((res)=>{
        this.customer = res;
      });
      this.cities = [
        { name: 'Comnbined User, February', code: 'NY' },
        { name: 'Rome', code: 'RM' },
        { name: 'London', code: 'LDN' },
        { name: 'Istanbul', code: 'IST' },
        { name: 'Paris', code: 'PRS' },
        { name: 'Rome', code: 'RM' },
        { name: 'London', code: 'LDN' },
        { name: 'Istanbul', code: 'IST' },
        { name: 'Paris', code: 'PRS' }
      ];
    }
}
