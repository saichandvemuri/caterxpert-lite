import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ServiceTabsRoutingModule } from './service-tabs-routing.module';
import { ServiceTabsComponent } from './service-tabs.component';
import { PrimeNgModule } from '../shared/prime-ng/prime-ng.module';
import { MenuComponent } from './menu/menu.component';
import { StaffingComponent } from './staffing/staffing.component';
import { AlcoholComponent } from './alcohol/alcohol.component';
import { BeveragesComponent } from './beverages/beverages.component';
import { EquipmentComponent } from './equipment/equipment.component';
import { DisposableComponent } from './disposable/disposable.component';
import { RentalsComponent } from './rentals/rentals.component';
import { DecorComponent } from './decor/decor.component';
import { AdditionalServiceComponent } from './additional-service/additional-service.component';


@NgModule({
  declarations: [
    ServiceTabsComponent,
    MenuComponent,
    StaffingComponent,
    AlcoholComponent,
    BeveragesComponent,
    EquipmentComponent,
    DisposableComponent,
    RentalsComponent,
    DecorComponent,
    AdditionalServiceComponent
  ],
  imports: [
    CommonModule,
    ServiceTabsRoutingModule,
    PrimeNgModule
  ]
})
export class ServiceTabsModule { }
