import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ServiceTabsComponent } from './service-tabs.component';

const routes: Routes = [{ path: '', component: ServiceTabsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServiceTabsRoutingModule { }
