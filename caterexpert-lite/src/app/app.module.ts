import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './core/header/header.component';
import { PrimeNgModule } from './shared/prime-ng/prime-ng.module';
import { EventListingComponent } from './core/event-listing/event-listing.component';
import { DataService } from './service/data.service';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { CreateCustomerComponent } from './core/create-customer/create-customer.component';
import { ApiService } from './service/api.service';
import { SharedModule } from './shared/shared/shared.module';
import { NgxSpinnerModule } from "ngx-spinner";
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpInterceptorService } from './service/http-interceptor.service';
import { SidebarComponent } from './core/sidebar/sidebar.component';
import { CalllogComponent } from './core/calllog/calllog.component';
import { ViewCalllogComponent } from './core/view-calllog/view-calllog.component';
import { ApprovalsComponent } from './core/approvals/approvals.component';
import { DatePipe } from '@angular/common';
import { BillServiceComponent } from './core/bill-service/bill-service.component';
import { BillInformationComponent } from './core/bill-information/bill-information.component';
import { ClosedDaysComponent } from './core/closed-days/closed-days.component';
import { FutureComponent } from './core/closed-days/future/future.component';
import { PastComponent } from './core/closed-days/past/past.component';
import { AllComponent } from './core/closed-days/all/all.component';
import { ECaterxpertOrdersComponent } from './core/e-caterxpert-orders/e-caterxpert-orders.component';
import { ProposalPrintComponent } from './core/proposal-print/proposal-print.component';
import { FinalizeMenuEditComponent } from './core/finalize-menu-edit/finalize-menu-edit.component';
import { ProposalImagesComponent } from './core/proposal-images/proposal-images.component';
import { SendMailComponent } from './core/send-mail/send-mail.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    EventListingComponent,
    CreateCustomerComponent,
    SidebarComponent,
    CalllogComponent,
    ViewCalllogComponent,
    ApprovalsComponent,
    BillServiceComponent,
    BillInformationComponent,
    ClosedDaysComponent,
    FutureComponent,
    PastComponent,
    AllComponent,
    ECaterxpertOrdersComponent,
    ProposalPrintComponent,
    FinalizeMenuEditComponent,
    ProposalImagesComponent,
    SendMailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    PrimeNgModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgxSpinnerModule
  ],
  providers: [DataService,ApiService,DatePipe,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true
    },],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],

})
export class AppModule { }
