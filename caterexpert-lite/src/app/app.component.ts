import { HttpClient } from '@angular/common/http';
import { HostListener } from '@angular/core';
import { Component } from '@angular/core';
import { ApiService } from './service/api.service';
import { HttpInterceptorService } from './service/http-interceptor.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'caterexpert-lite';
  @HostListener("window:beforeunload", ["$event"]) unloadHandler(event: Event) {
    this.storeData();
  }
  constructor(public apiService: ApiService, public http: HttpClient, public interceptorService: HttpInterceptorService) {
    // console.log("App component")

  }
  ngOnInit() {
    try {
      this.http.get("assets/url.json").subscribe(
        response => {
          let responseData: any = response;
          let webserviceURL = responseData.webserviceURL;
          localStorage.setItem("webserviceURL", webserviceURL);
          this.interceptorService.baseUrl = responseData.webserviceURL;
          this.interceptorService.seturl();
        }
      )
    }
    catch (error) {
      console.log(error);
    }
  }
  public storeData() {
    // store data into local storage before browser refresh

    sessionStorage.setItem('loginresponse', JSON.stringify(this.apiService.loginResponse));

  }
}
