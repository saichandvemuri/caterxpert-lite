import { Injectable } from '@angular/core';
import { Location } from '@angular/common'
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { Observable } from 'rxjs';
import Swal from 'sweetalert2';



@Injectable({
  providedIn: 'root'
})
export class ApiService {
  public headerName: string = null;
  public sideBarList: Array<any> = [];
  public loginResponse: any;
  constructor(private loaction: Location, private http: HttpClient) {
    this.loginResponse = JSON.parse(sessionStorage.getItem('loginresponse'))
    console.log("apiservice")
    sessionStorage.removeItem('loginresponse');
  }

  public authenticateUser(obj, caterid): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        "clientId": caterid,

      }),
    }
    return this.http.post(`/authenticationrestapi/authenticateUser`, obj, options).pipe(catchError(this.errorHandler))
  }
  public getAfterLoginValues(): Observable<any> {
    return this.http.post(`/authenticationrestapi/getAfterLoginValues`, { "userId": this.loginResponse.loggedInUser }).pipe(catchError(this.errorHandler))
  }

  public getEventSearchLookUp(): Observable<any> {
    return this.http.get(`/salesrestapi/getEventSearchLookUp`).pipe(catchError(this.errorHandler));
  }
  public getEventListWithSearch(obj): Observable<any> {
    return this.http.post(`/salesrestapi/getEventListWithSearch`, obj).pipe(catchError(this.errorHandler));
  }
  public getEventLookups(obj): Observable<any> {
    return this.http.post(`/salesrestapi/getEventLookups`, obj).pipe(catchError(this.errorHandler));
  }
  public saveEvent(obj): Observable<any> {
    return this.http.post(`/salesrestapi/saveEvent`, obj).pipe(catchError(this.errorHandler))
  }
  public getUserPreferences(): Observable<any> {
    return this.http.post(`/authenticationrestapi/getUserPreferences`, { "userId": this.loginResponse.loggedInUser }).pipe(catchError(this.errorHandler))
  }
  public getFilterPreference(): Observable<any> {
    return this.http.post(`/authenticationrestapi/getFilterPreference`, { "userId": this.loginResponse.loggedInUser }).pipe(catchError(this.errorHandler))
  }
  public saveUserPreferences(obj): Observable<any> {
    return this.http.post(`/authenticationrestapi/saveUserPreferences`, obj).pipe(catchError(this.errorHandler));
  }
  public saveFilterPreference(obj): Observable<any> {
    return this.http.post(`/authenticationrestapi/saveFilterPreferences`, obj).pipe(catchError(this.errorHandler));
  }
  public getCustomerLookup(): Observable<any> {
    return this.http.get(`/salesrestapi/getCustomerLookup`).pipe(catchError(this.errorHandler));
  }
  public saveCustomer(obj): Observable<any> {
    return this.http.post(`/saveCustomer`, obj).pipe(catchError(this.errorHandler));
  }
  public customerAutoComplete(): Observable<any> {
    return this.http.get(`/salesrestapi/customerAutoComplete`).pipe(catchError(this.errorHandler));
  }
  public getSystemPreferences(): Observable<any> {
    return this.http.get(`/authenticationrestapi/getSystemPreferences`).pipe(catchError(this.errorHandler));
  }
  public saveSystemPreferences(obj): Observable<any> {
    return this.http.post(`/authenticationrestapi/saveSystemPreferences`, obj).pipe(catchError(this.errorHandler));
  }

  public getCXPLiteAbout(): Observable<any> {
    return this.http.get(`/authenticationrestapi/getCXPLiteAbout`).pipe(catchError(this.errorHandler));
  }

  public saveSuperAdminLoginAction(obj): Observable<any> {
    return this.http.post(`/authenticationrestapi/saveSuperAdminLoginAction`, obj).pipe(catchError(this.errorHandler));
  }
  public changePassword(obj): Observable<any> {
    return this.http.post(`/authenticationrestapi/changePassword`, obj).pipe(catchError(this.errorHandler));
  }











  private errorHandler(error: HttpErrorResponse) {
    console.log("error in API service", error);
    Swal.fire({
      html: error.error,
      icon: 'error'
    })
    return throwError(error);
  }

  /**
 * SetheaderName is used to set Dynamic heading in header component
 */
  public SetheaderName(value: string, headerArray?: Array<any>) {
    this.headerName = value;
    this.sideBarList = headerArray ? headerArray : this.sideBarList;
    // console.log(this.sideBarList.length)
  }

  /**
   * previousRoute is used to navigate previous route
   */
  public previousRoute() {
    this.loaction.back();
  }
  public headerData() {
    const options = {
      headers: new HttpHeaders({
        "clientId": this.loginResponse.clientId,
        "sessionId": this.loginResponse.sessionId,
        "userId": this.loginResponse.loggedInUser
      }),
    }
    return options;
  }
}
