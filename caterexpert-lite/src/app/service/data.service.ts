import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class DataService {
  private _apiurl: string = "https://jsonplaceholder.typicode.com/posts";
  constructor( private http: HttpClient) { }
  getdata(): Observable<any> {
    return this.http.get<any>(`${this._apiurl}`);
}
}
