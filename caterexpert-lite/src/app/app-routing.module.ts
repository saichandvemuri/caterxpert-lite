import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CalllogComponent } from './core/calllog/calllog.component';
import { CreateCustomerComponent } from './core/create-customer/create-customer.component';
import { EventListingComponent } from './core/event-listing/event-listing.component';
import { HeaderComponent } from './core/header/header.component';
import { ViewCalllogComponent } from './core/view-calllog/view-calllog.component';
import { LoginComponent } from './login/login.component';
import { AuthenticationGuard } from './service/authentication.guard';
import { ApprovalsComponent } from './core/approvals/approvals.component';
import { BillServiceComponent } from './core/bill-service/bill-service.component';
import { BillInformationComponent } from './core/bill-information/bill-information.component';
import { ClosedDaysComponent } from './core/closed-days/closed-days.component';
import { ECaterxpertOrdersComponent } from './core/e-caterxpert-orders/e-caterxpert-orders.component';
import { ProposalPrintComponent } from './core/proposal-print/proposal-print.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: "full" },
  { path: "login", component: LoginComponent },
  {path:"bill-service",component:BillServiceComponent},
  {path:"bill-info",component:BillInformationComponent},
  {path:"closed-days",component:ClosedDaysComponent},
  {path:"e-cater-orders",component:ECaterxpertOrdersComponent},
  {path:"proposal-print",component:ProposalPrintComponent},
  // { path: "eventslisting", canActivate: [AuthenticationGuard], component: EventListingComponent },
  // { path: "createevent", canActivate: [AuthenticationGuard], component: CreateEventComponent },
  // { path: "c-customer", canActivate: [AuthenticationGuard], component: CreateCustomerComponent },
  { path: "call-log", canActivate: [AuthenticationGuard], component: CalllogComponent },
  { path: "view-call-log", canActivate: [AuthenticationGuard], component: ViewCalllogComponent },
  { path: 'customer', canActivate: [AuthenticationGuard], loadChildren: () => import('./customer/customer.module').then(m => m.CustomerModule) },
  { path: 'user', canActivate: [AuthenticationGuard], loadChildren: () => import('./core/user/user.module').then(m => m.UserModule) },
  { path: 'events', loadChildren: () => import('./events/events.module').then(m => m.EventsModule) },
  // { path: "eventslisting", component: EventListingComponent },
  { path: "c-customer", component: CreateCustomerComponent },
  // { path: "call-log", component: CalllogComponent },
  // { path: "view-call-log", component: ViewCalllogComponent },
  { path: "approvals", component: ApprovalsComponent },
  { path: 'user', loadChildren: () => import('./core/user/user.module').then(m => m.UserModule) },
  { path: 'events', loadChildren: () => import('./events/events.module').then(m => m.EventsModule) },
  { path: 'service-tabs', loadChildren: () => import('./service-tabs/service-tabs.module').then(m => m.ServiceTabsModule) },


  { path: '**', redirectTo: '/events/event-list', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
